# 电子商城需求

## 一、顾客端大致需求原型（只需要PC端）

<strong>商城首页</strong>

![图裂了！](./imgs/img1.png)

![图裂了！](./imgs/img1-1.png)

<strong>商城商品详情</strong>

![图裂了！](./imgs/img2.png)

![图裂了！](./imgs/img2-1.png)

<strong>商城购物车</strong>

![图裂了！](./imgs/img3.png)

<strong>商城购物车结算</strong>

![图裂了！](./imgs/img4.png)

<strong>商城收银台</strong>

![图裂了！](./imgs/img5.png)


## 二、商城后台主要功能（分为系统管理员、供应商，各自有操作）

1. 商城系统的管理（包括但不限于商城名称、Logo、备案信息、用户、角色、权限）
2. 供应商的管理（包括但不限于供应商的新增、修改、删除、禁用、排序，供应商信息的维护等）
3. 客户的管理（包括但不限于用户注册、重置密码、禁用、删除限制登录，用户信息的维护，用户收件地址维护等）
3. 商品品类的管理（包括但不限于品类的增加、删除、禁用、排序，注意品类有上下级关系）
4. 商品的管理（包括但不限于商品的增加、删除、上架、下架、排序，商品信息的维护等，商品具有品类属性）
5. 订单的管理（包括但不限于查看各类订单）
6. 支付方式的管理
